package bashima.cs.unc.checkaudiosamplerate;

;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    ToggleButton toggle = null;
    TextView tv1;
    TextView tv2;
    TextView tv3;
    TextView tv4;
    TextView tv5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setToggleButtonListener();
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tv4 = (TextView) findViewById(R.id.tv4);
        tv5 = (TextView) findViewById(R.id.tv5);
    }

    void setToggleButtonListener() {
        toggle = (ToggleButton) findViewById(R.id.toggleButton);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if(!getValidSampleRates(8000))
                    {
                        tv1.setTextColor(getResources().getColor(R.color.red));
                    }
                    else
                    {
                        tv1.setTextColor(getResources().getColor(R.color.green));
                    }
                    if(!getValidSampleRates(11025))
                    {
                        tv2.setTextColor(getResources().getColor(R.color.red));
                    }
                    else
                    {
                        tv2.setTextColor(getResources().getColor(R.color.green));
                    }
                    if(!getValidSampleRates(16000))
                    {
                        tv3.setTextColor(getResources().getColor(R.color.red));
                    }
                    else
                    {
                        tv3.setTextColor(getResources().getColor(R.color.green));
                    }
                    if(!getValidSampleRates(22050))
                    {
                        tv4.setTextColor(getResources().getColor(R.color.red));
                    }
                    else
                    {
                        tv4.setTextColor(getResources().getColor(R.color.green));
                    }
                    if(!getValidSampleRates(44100))
                    {
                        tv5.setTextColor(getResources().getColor(R.color.red));
                    }
                    else
                    {
                        tv5.setTextColor(getResources().getColor(R.color.green));
                    }
                } else {
                    tv1.setTextColor(getResources().getColor(R.color.black));
                    tv2.setTextColor(getResources().getColor(R.color.black));
                    tv3.setTextColor(getResources().getColor(R.color.black));
                    tv4.setTextColor(getResources().getColor(R.color.black));
                    tv5.setTextColor(getResources().getColor(R.color.black));

                }
            }
        });
    }
    public boolean getValidSampleRates(int rate) {

            int bufferSize = AudioRecord.getMinBufferSize(rate, AudioFormat.CHANNEL_CONFIGURATION_DEFAULT, AudioFormat.ENCODING_PCM_16BIT);
            if (bufferSize > 0) {
                // buffer size is valid, Sample rate supported
                return true;

            }
        return false;
    }

}
